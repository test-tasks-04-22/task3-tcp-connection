﻿using System.Net;
using System.Net.Sockets;
using System.Text;

try
{
    SendMessage(8888);
}
catch (Exception exception)
{
    Console.WriteLine(exception.ToString());
}

static void SendMessage(int port)
{
    IPHostEntry host = Dns.GetHostEntry("localhost");
    IPAddress address = host.AddressList[0];
    Socket sender = BuildSender(address);
    sender.Connect(new IPEndPoint(address, port));

    while (true)
    { 
        string letter = Console.ReadKey().KeyChar.ToString();
        sender.Send(Encoding.UTF8.GetBytes(letter));
    }
}

static Socket BuildSender(IPAddress address)
{
    return new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
}

﻿using System.Net;
using System.Net.Sockets;
using System.Text;

IPHostEntry host = Dns.GetHostEntry("localhost");
IPAddress address = host.AddressList[0];
IPEndPoint endPoint = new IPEndPoint(address, 8888);
Socket listener = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

try
{
    listener.Bind(endPoint);
    listener.Listen(15);

    Console.WriteLine($"Waiting for a connection on {endPoint}");
    Socket handler = listener.Accept();
    while (true)
    {
        byte[] bytes = new byte[1024];
        int bytesReceived = handler.Receive(bytes);
        Console.Write(Encoding.UTF8.GetString(bytes, 0, bytesReceived));
    }
}
catch (Exception exception)
{
    Console.WriteLine(exception.ToString());
}
